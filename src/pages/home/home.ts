import { LeituraPage } from './../leitura/leitura';
import { Component } from '@angular/core';
import { NavController, ToastController, NavParams, AlertController, Platform, LoadingController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { File } from '@ionic-native/file';
import { Uid } from '@ionic-native/uid';
import { ListProvider } from '../../providers/list/list';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Device } from '@ionic-native/device';
const androidPath = "file:////storage/emulated/0/Android/data/io.tecdom.starter/files/";
const path = "file:////storage/emulated/0/Android/data/io.tecdom.starter/";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loteLength: number;
  uuid_ios: any;
  constructor(private device: Device,private loadCtrl: LoadingController, private uid: Uid, private listProvider: ListProvider, private platform: Platform, private alertCtrl: AlertController, private file: File, private db: DbProvider, public navCtrl: NavController, private naveParams: NavParams, private toastCtrl: ToastController) {
  //  this.exibe();
    if (this.naveParams.get('loteLength')) {
      this.loteLength = this.naveParams.get('loteLength');
    }
  }

  getdb() {
    this.db.getLoginStatus().then(resp => {
      console.log(resp);
    });
  }
/*
  exibe(){
    this.uuid.get().then(resp => {
      this.uuid_ios = resp;
    });
    console.log(uuid_ios);
  }

  exibePlataforma() {
    let r = this.platform.is('android');
    console.log(r);
  }
*/
  tratarArquivo() {
    let autorizado: boolean = false;
    if (this.platform.is('android')) {
      this.file.readAsText(androidPath, 'lista.txt').then((resp) => {

        resp = resp + '';
        let imei = this.uid.IMEI;
        let r = resp.split('\n');
        //this.db.setLoginStatus(false);
        for (var e = 0; e < r.length; e++) {
          if (r[e] == imei) {
            autorizado = true;
            break;
          }
        }


        if (autorizado) {
          this.db.setLoginStatus(true);
          if (this.loteLength) {
            console.log(this.loteLength);
            this.db.setLoginStatus(true);
            this.navCtrl.push(LeituraPage, {
              loteLength: this.loteLength
            });
          } else {
            this.navCtrl.push(LeituraPage, {
              loteLength: 0
            });
          }
        } else {
          this.db.setLoginStatus(false);
          this.toastCtrl.create({
            message: 'Dispositivo não autorizado!',
            position: 'top',
            cssClass: 'toastCtrl',
            duration: 3000
          }).present();
        }

      });
    }
    if (this.platform.is('ios')) {
      this.file.readAsText(this.file.applicationStorageDirectory, 'lista.txt').then((resp) => {

        resp = resp + '';
        let imei = this.device.uuid;
        
        let r = resp.split('\n');
        //this.db.setLoginStatus(false);
        for (var e = 0; e < r.length; e++) {
          if (r[e] == imei) {
            autorizado = true;
          }
        }


        if (autorizado) {
          this.db.setLoginStatus(true);
          if (this.loteLength) {
            console.log(this.loteLength);
            this.db.setLoginStatus(true);
            this.navCtrl.push(LeituraPage, {
              loteLength: this.loteLength
            });
          } else {
            this.navCtrl.push(LeituraPage, {
              loteLength: 0
            });
          }
        } else {
          this.db.setLoginStatus(false);
          this.toastCtrl.create({
            message: 'Dispositivo não autorizado!',
            position: 'top',
            cssClass: 'toastCtrl',
            duration: 3000
          }).present();
        }

      });
    }

  }

  submit() {
    this.db.getLoginStatus().then(resp => {

      if (resp == true) {
        console.log('entrou resp == true');
        if (this.loteLength) {
          console.log(this.loteLength);
          this.loadCtrl.create({
            content: 'carregando...',
            duration: 2000
          }).present();
          this.navCtrl.push(LeituraPage, {
            loteLength: this.loteLength
          });
        } else {
          this.navCtrl.push(LeituraPage, {
            loteLength: 0
          });
        }
      } else {
        if (resp == null) {
          console.log('entrou resp == null');
          this.listProvider.downloadList();
          this.tratarArquivo();

        } else {
          this.listProvider.downloadList();
          this.tratarArquivo();
        }
      }
    }).catch(err => {

    });

  }

  limpar() {
    this.alertCtrl.create({
      title: 'Atenção',
      subTitle: 'Isso eliminiará todos os registros do lote',
      message: 'Tem certeza que deseja limpar todos os lotes?',
      buttons: [

        {
          text: 'Sim',
          role: 'dismiss',
          handler: () => {
            this.db.getItems().then(resp => {
              console.log(resp);
              let array: Array<{ filename: string, path: string }> = [];
              for (var e = 0; e < resp.length; e++) {
                array.push(resp[e]);
              }
              this.deleteFile(array);
            });
          }
        },
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    }).present();
    this.loteLength = null;




    //this.deleteRecursively(array);
  }


  deleteFileFromDb(array) {
    this.db.deleteAllItems(array);
    this.db.getItems().then(resp => {
      console.log(resp);
    })
  }

  deleteFile(array) {
    if (this.platform.is('android')) {
      for (var e = 0; e < array.length; e++) {

        this.file.removeFile(androidPath, array[e].filename);
      }
    }
    if(this.platform.is('ios')){
      for (var i = 0; i < array.length; i++) {
        this.file.removeFile(this.file.applicationStorageDirectory, array[i].filename);
      }
    }

    this.deleteFileFromDb(array);
  }

  deleteRecursively(array) {
    this.file.removeRecursively(path, 'files').then(resp => {
      console.log(resp);
      this.deleteFileFromDb(array);
    });
  }

}
