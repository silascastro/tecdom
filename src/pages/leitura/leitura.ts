import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ToastController, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File } from '@ionic-native/file';
import { HomePage } from '../home/home';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EnviarEmailPage } from '../enviar-email/enviar-email';
import { DbProvider } from '../../providers/db/db';
import { Device } from '@ionic-native/device';
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';


const androidPath = "file:////storage/emulated/0/Android/data/io.tecdom.starter/files/";
//const path = "file:////storage/emulated/0/Android/data/io.ionic.starter/";
//@IonicPage()
@Component({
  selector: 'page-leitura',
  templateUrl: 'leitura.html',
})
/**/
export class LeituraPage {
  cod: any;
  input: number;
  loteLength: number;
  lote: Array<any> = [];
  showInput: boolean = false;
  defaultMult = 1;
  multAllow: number;
  mult: FormGroup;
  arrayLote = [];
  //filename: string;
  imei: any;
  constructor(private device: Device,private uid: Uid, private androidPermissions: AndroidPermissions, private alertCtrl: AlertController, private db: DbProvider, private fb: FormBuilder, private toastCtrl: ToastController, private platform: Platform, private file: File, private barCodeScanner: BarcodeScanner, public navCtrl: NavController, public navParams: NavParams) {
    this.loteLength = this.navParams.get('loteLength');
    console.log('lote: ', this.loteLength);
    this.initializeMult();
  }

  initializeMult() {
    this.mult = this.fb.group({
      mult: [this.defaultMult, Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeituraPage');

    this.InitializeArrayLote();
    //this.teste1();
  }

  InitializeArrayLote() {
    for (var e = 0; e < this.loteLength; e++) {
      this.arrayLote.push(e + 1);
    }
    this.multAllow = this.loteLength;
    console.log('multAllow: ', this.multAllow);
    console.log(this.mult.controls['mult'].value);
  }


  showMessage() {
    this.toastCtrl.create({
      message: 'Tamanho máximo do lote!',
      duration: 3000,
      cssClass: 'toastCtrl'
    }).present();
  }

  showMessageInput() {
    this.toastCtrl.create({
      message: 'Digite um valor!',
      duration: 3000,
      cssClass: 'toastCtrl'
    }).present();
  }

  showMessageMult() {
    this.toastCtrl.create({
      message: 'O multiplicador não pode ser 0',
      duration: 3000,
      cssClass: 'toastCtrl'
    }).present();
  }

  verifyAllowebleInput() {
    if (this.loteLength > 0) {
      if (this.input != null) {
        if (this.mult.controls['mult'].value > 1) {
          if ((this.multAllow - this.mult.controls['mult'].value) >= 0) {
            this.multAllow = this.multAllow - this.mult.controls['mult'].value;
            this.inputMoreThanOne(this.mult.controls['mult'].value);
          } else {
            this.input = null;
            this.showMessage();
          }
        } else {
          if (this.multAllow == 0) {
            this.input = null;
            this.showMessage();
          } else {
            if ((this.multAllow - this.mult.controls['mult'].value) >= 0) {
              this.multAllow = this.multAllow - this.mult.controls['mult'].value;
              this.getInput();
            } else {
              this.input = null;
              this.showMessage();
            }
          }
        }
      } else {
        this.showMessageInput();
      }
    } else {
      if (this.input != null) {
        if (this.mult.controls['mult'].value > 1) {
          this.inputMoreThanOne(this.mult.controls['mult'].value);
        } else {
          this.getInput();
        }
      } else {
        this.showMessageInput();
      }
    }

  }

  inputMoreThanOne(times) {
    for (var e = 0; e < times; e++) {
      this.lote.push(this.input + String.fromCharCode(13) + String.fromCharCode(10));
    }
    this.input = null;
  }


  getInput() {
    /* cada linha do arquivo é finalizada com um código ASCII 13 + \n  */
    this.lote.push(this.input + String.fromCharCode(13) + String.fromCharCode(10));
    this.input = null;

  }


  verifyAllowebleScan() {
    if (this.loteLength > 0) {
      if (this.mult.controls['mult'].value > 1) {
        if ((this.multAllow - this.mult.controls['mult'].value) >= 0) {
          console.log(this.multAllow);

          this.scanearMultMoreThanOne(this.mult.controls['mult'].value);
        } else {

          this.showMessage();
        }
      } else {

        if (this.multAllow == 0) {
          this.showMessage();
        } else {
          if ((this.multAllow - this.mult.controls['mult'].value) >= 0) {

            this.scanear();
          } else {
            this.showMessage();
          }
        }
      }
    } else {
      if (this.mult.controls['mult'].value > 1) {
        this.scanearMultMoreThanOne(this.mult.controls['mult'].value);
      } else {
        this.scanear();
      }
    }
  }

  scanear() {
    console.log('entrou em scanear');

    let scanOptions = { orientation: 'portrait' };
    this.barCodeScanner.scan(scanOptions).then((barcode) => {
      if (!barcode.cancelled) {
        if (this.loteLength > 0) {
          this.multAllow = this.multAllow - this.mult.controls['mult'].value;
        }
        this.lote.push((barcode.text) + String.fromCharCode(13) + String.fromCharCode(10));
      }
    }).catch(err => {
      console.log(err);
      if (this.loteLength > 0) {
        this.multAllow = this.multAllow + this.mult.controls['mult'].value;
      }
    });

  }

  scanearMultMoreThanOne(times) {

    let scanOptions = { orientation: 'portrait' };
    this.barCodeScanner.scan(scanOptions).then((barcode) => {
      if (!barcode.cancelled) {
        if (this.loteLength > 0) {
          this.multAllow = this.multAllow - this.mult.controls['mult'].value;
        }
        for (var e = 0; e < times; e++) {
          this.lote.push((barcode.text) + String.fromCharCode(13) + String.fromCharCode(10));
        }
      }

    }).catch(err => {
      console.log(err);
      if (this.loteLength > 0) {
        this.multAllow = this.multAllow + this.mult.controls['mult'].value;
      }
    });

  }

  novoLote() {
    if (this.lote.length != 0) {
      this.readLote();

      this.navCtrl.setRoot(HomePage, { loteLength: this.loteLength });
    }
  }

  async getImei() {

    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );

    if (!hasPermission) {
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!result.hasPermission) {

        throw new Error('Permissions required');
      }

      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }

    return this.uid.MAC;
    //resolve(this.uid.MAC);
  }

  getDate() {
    let str: string;
    let date = new Date();

    str = (date.getFullYear().toString()).slice(-2);


    if (date.getMonth() < 10) {
      str = str + "0" + date.getMonth();
    } else {
      str = str + date.getMonth();
    }


    if ((date.getDate()) < 10) {
      str = str + "0" + (date.getDate()).toString();
    } else {
      str = str + date.getDate().toString();
    }

    return str;
  }

  getLote() {
    let loteNumber;
    let r = localStorage.getItem('lote');
    if (r) {
      if (r == '999') {
        loteNumber = "001";
        localStorage.setItem('lote', '1');
      }
      else {
        if (parseInt(r) + 1 < 100) {
          if (parseInt(r) + 1 < 10) {
            loteNumber = "00" + r;
          }
          else {
            loteNumber = "0" + r;
          }
        } else {
          loteNumber = r;
        }
        var s = parseInt(r) + 1;
        localStorage.setItem('lote', s.toString());
      }
    }
    else {
      localStorage.setItem('lote', '1');
      loteNumber = "001";
    }

    return loteNumber;
  }

  formatString() {
    var name;
    if (this.platform.is('android')) {

      let imei = this.uid.IMEI;
      console.log(imei);
      console.log(this.getDate());
      this.db.setLote();
      let loteNumber = this.getLote();
      name = "C" + imei.slice(0, 2) + this.getDate() +"."+ loteNumber;
    }

    if (this.platform.is('ios')) {
      let imei = this.device.uuid;
      this.db.setLote();
      let loteNumber = this.getLote();
      name = "C" + imei.slice(0,2) + this.getDate() +"." +loteNumber;
    }

    return name;
  }

  readLote() {
    //this.lote.push(String.fromCharCode(26)+""+ String.fromCharCode(13));
    let str = new Blob(this.lote, { type: 'text/html' });
    console.log(str);
    this.writeFile(this.createFile(), str);
  }

  createFile() {
    //let iso = new Date().toISOString();
    let filename = this.formatString();
    console.log(filename);
    if (this.platform.is('android')) {
      this.file.createFile(androidPath, filename, true).then(resp => { }).catch(err => { });
    }
    if (this.platform.is('ios')) {
      this.file.createFile(this.file.applicationStorageDirectory, filename, true).then(resp => { }).catch(err => { });
    }
    return filename;

  }

  writeFile(filename, text) {
    if (this.platform.is('android')) {
      this.file.writeExistingFile(androidPath, filename, text).then(resp => { console.log(resp) }).catch(err => { console.log(err) });
      //armazena no storage
      this.db.addItem({
        filename: filename,
        path: androidPath
      }).then(_ => { }).catch(_ => { });
    }
    if (this.platform.is('ios')) {
      this.file.writeExistingFile(this.file.applicationStorageDirectory, filename, text).then(resp => { console.log(resp) }).catch(err => { console.log(err) });
      //armazena no storage
      this.db.addItem({
        filename: filename,
        path: this.file.applicationStorageDirectory
      }).then(_ => { }).catch(_ => { });
    }
  }

  hiddenInput() {
    if (this.showInput) {
      this.input = null;
      this.showInput = false;
    } else {
      this.showInput = true;
    }

  }

  finalizar() {
    this.readLote();
    this.navCtrl.setRoot(EnviarEmailPage);
  }

  recontar() {
    this.alertCtrl.create({
      title: 'Atenção',
      message: 'Tem certeza que deseja recontar o lote atual?',
      subTitle: 'Isso eliminiará todos os registros do lote',
      buttons: [
        {
          text: 'Sim',
          role: 'dismiss',
          handler: () => {
            this.lote = [];
            this.multAllow = this.loteLength;
          }
        },
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {

          }
        }
      ]

    }).present();

  }




}
