import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, Platform } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { DbProvider } from '../../providers/db/db';
import { File } from '@ionic-native/file';

const androidPath = "file:////storage/emulated/0/Android/data/io.tecdom.starter/files/";
const path = "file:////storage/emulated/0/Android/data/io.tecdom.starter/";

@Component({
  selector: 'page-enviar-email',
  templateUrl: 'enviar-email.html',
})
export class EnviarEmailPage {
  arrayFiles: Array<string> = [];
  to: string;
  constructor(private platform: Platform, private alertCtrl: AlertController, private toastCtrl: ToastController, private file: File, private db: DbProvider, private emailComposer: EmailComposer, public navCtrl: NavController, public navParams: NavParams) {
    
    this.getFiles();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnviarEmailPage');
  }

  goToHome() {
    this.alertCtrl.create({
      title: 'Atenção',
      message: 'Tem certeza que deseja fechar?',
      buttons: [
        {
          text: 'Sim',
          role: 'dismiss',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        },
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {

          }
        }
      ]

    }).present();

  }

  getFiles() {
    this.db.getItems().then(resp => {
      console.log(resp);
      for (var e = 0; e < resp.length; e++) {
        this.arrayFiles.push(resp[e].path + resp[e].filename);
      }
      console.log(this.arrayFiles);
    });

  }

  validaEmail() {
    if (this.to && (this.to.length) > 6) {
      this.sendEmail();

    } else {
      this.toastCtrl.create({
        message: 'Digite um email',
        duration: 3000,
        position: 'top',
        cssClass: 'toastCtrl'
      }).present();
    }
    /**/
  }

  sendEmail() {

    let email = {
      to: this.to,
      cc: 'pedrocesar@tecdom.com.br',
      //bcc: ['john@doe.com', 'jane@doe.com'],
      /*attachments: [
        'file://img/logo.png',
        'res://icon.png',
        'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        'file://README.pdf'
      ],*/
      attachments: this.arrayFiles,
      subject: 'Códigos de barras lidos',
      body: 'seguem os anexos',
      isHtml: false
    };
    //console.log('file:////storage/emulated/0/Android/data/io.tecdom.starter/files/2019-04-060559379Z5533441321351469.txt');
    this.emailComposer.open(email).then(resp => {
      this.goToHome();
    }).catch(err => {

    });
  }

  delete() {
    let array: Array<{ filename: string, path: string }> = [];

      
      this.db.getItems().then(resp => {
        //console.log(resp);
        for (var e = 0; e < resp.length; e++) {
          array.push(resp[e]);
        }
      });
    

    this.deleteFile(array);
    //this.deleteRecursively(array);

  }

  deleteFileFromDb(array) {
    this.db.deleteAllItems(array);
  }

  deleteFile(array) {
    if (this.platform.is('android')) {
      for (var e = 0; e < array.length; e++) {

        this.file.removeFile(androidPath, array[e].filename);
      }
    }
    if(this.platform.is('ios')){
      for (var i = 0; i < array.length; i++) {
        this.file.removeFile(this.file.applicationStorageDirectory, array[i].filename);
      }
    }

    this.deleteFileFromDb(array);
  }

  deleteRecursively(array) {
    this.file.removeRecursively(path, 'files').then(resp => {
      console.log(resp);
      this.deleteFileFromDb(array);
    });
  }

}
