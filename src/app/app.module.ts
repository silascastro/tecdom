import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { IonicStorageModule } from '@ionic/storage';
import { EmailComposer } from '@ionic-native/email-composer';
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Device } from '@ionic-native/device';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LeituraPage } from '../pages/leitura/leitura';
import { DbProvider } from '../providers/db/db';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { EnviarEmailPage } from '../pages/enviar-email/enviar-email';
import { ListProvider } from '../providers/list/list';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LeituraPage,
    EnviarEmailPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LeituraPage,
    EnviarEmailPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Uid,
    Device,
    UniqueDeviceID,
    AndroidPermissions,
    BarcodeScanner,
    EmailComposer,
    File,
    FileTransfer,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DbProvider,
    ListProvider
  ]
})
export class AppModule { }
