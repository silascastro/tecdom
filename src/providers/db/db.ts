import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


export interface Item {
  path: any;
  filename: any;
}

const ITEMS_KEY = 'barCode';
const LOTE_KEY = 'lote';
@Injectable()
export class DbProvider {

  constructor(public storage: Storage) {

  }

  addItem(item: Item) {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (items) {
        items.push(item);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }

  getItems() {
    return this.storage.get(ITEMS_KEY).then(() => this.storage.get(ITEMS_KEY));
  }

  deleteItem(filename: string) {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let toKeep: Item[] = [];

      for (let i of items) {
        if (i.filename !== filename) {
          toKeep.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, toKeep);
    })
  }

  deleteAllItems(array) {
    this.storage.set(ITEMS_KEY, []);
  }

  setLote() {
    this.storage.get(LOTE_KEY).then((item) => {
      if (item) {
        if(item+1<999)
        {
          this.storage.set(LOTE_KEY, item+1);
        }else{
          this.storage.set(LOTE_KEY, 1);
        }
/*        if (item < 100) {
          if (item < 10) {
            if ((item + 1) < 10) {
              this.storage.set(LOTE_KEY, "00" + item+1);
            }
            else {
              this.storage.set(LOTE_KEY, "0" + item+1);
            }

          }
          else {
            if ((item + 1) < 100) {
              this.storage.set(LOTE_KEY, "0" + item+1);
            }else{
              this.storage.set(LOTE_KEY,item+1);
            }
          }
        } else {
          this.storage.set(LOTE_KEY, item+1);
        }*/
      } else {
        this.storage.set(LOTE_KEY,1);

      }
    });
  }

  getLote() {
    return this.storage.get(LOTE_KEY).then(() => this.storage.get(LOTE_KEY)).catch(err => console.log(err));
  }

  setLoginStatus(status) {
  this.storage.set('LOGIN_STATUS', status);
  }

  getLoginStatus() {
    return this.storage.get('LOGIN_STATUS').then(_ => this.storage.get('LOGIN_STATUS'));
  }
}
