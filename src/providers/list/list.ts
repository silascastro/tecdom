import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';

//const fileTransfer:FileTransferObject = FileTransfer.create();

@Injectable()
export class ListProvider {
  fileTransfer: FileTransferObject;
  constructor(private platform: Platform, public http: Http, private transfer: FileTransfer, private file: File) {
    //console.log('Hello ListProvider Provider');
    this.fileTransfer = this.transfer.create();
  }

  downloadList() {
    if (this.platform.is('android')) {
      this.file.checkFile('file:////storage/emulated/0/Android/data/io.tecdom.starter/files/', 'lista.txt').then(resp => {
        //console.log('arquivo já existe!');
        const url = 'http://www.tecdom.com.br/leitor/lista.txt';
        this.fileTransfer.download(url, 'file:////storage/emulated/0/Android/data/io.tecdom.starter/files/' + 'lista.txt').then((entry) => {
          console.log('Donwload complete: ' + entry.toURL());
          //sthis.homePage.tratarArquivo();
        }, err => {
          console.log('erro ao baixar arquivo');
        });
      }).catch(err => {
        //console.log('erro');
        const url = 'http://www.tecdom.com.br/leitor/lista.txt';
        this.fileTransfer.download(url, 'file:////storage/emulated/0/Android/data/io.tecdom.starter/files/' + 'lista.txt').then((entry) => {
          console.log('Donwload complete: ' + entry.toURL());
          //sthis.homePage.tratarArquivo();
        }, err => {
          console.log('erro ao baixar arquivo');
        });
      });
    }

    if(this.platform.is('ios')){
      this.file.checkFile(this.file.applicationStorageDirectory, 'lista.txt').then(resp => {
        //console.log('arquivo já existe!');
        const url = 'http://www.tecdom.com.br/leitor/lista.txt';
        this.fileTransfer.download(url, this.file.applicationStorageDirectory + 'lista.txt').then((entry) => {
          console.log('Donwload complete: ' + entry.toURL());
          //sthis.homePage.tratarArquivo();
        }, err => {
          console.log('erro ao baixar arquivo');
        });
      }).catch(err => {
        //console.log('erro');
        const url = 'http://www.tecdom.com.br/leitor/lista.txt';
        this.fileTransfer.download(url,  this.file.applicationStorageDirectory + 'lista.txt').then((entry) => {
          console.log('Donwload complete: ' + entry.toURL());
          //sthis.homePage.tratarArquivo();
        }, err => {
          console.log('erro ao baixar arquivo');
        });
      });
    }


  }

}
